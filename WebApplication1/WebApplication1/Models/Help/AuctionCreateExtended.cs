﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.Extended
{
    public class AuctionCreateExtended
    {
        [Required]
        [StringLength(50)]
        public string naziv { get; set; }

        public int trajanje { get; set; }

        public int pocetnaCena { get; set; }

        [DisplayName("Upload Photo")]
        public String imagePath { get; set; }

        [Required]
        public HttpPostedFileBase imageFile { get; set; }

        
    }
}