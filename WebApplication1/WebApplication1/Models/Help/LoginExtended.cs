﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class LoginExtended
    {
        [Required]
        [Display(Name = "E-mail")]
        [StringLength(50)]
        public string mejl { get; set; }

        [Required]
        [Display(Name = "Password")]
        [StringLength(50)]
        public string lozinka { get; set; }
    }
}