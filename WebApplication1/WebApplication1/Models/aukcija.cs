namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("aukcija")]
    public partial class aukcija
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public aukcija()
        {
            ponudas = new HashSet<ponuda>();
        }

        [Key]
        public Guid GUID { get; set; }

        [Required]
        [StringLength(50)]
        public string naziv { get; set; }

        [Column(TypeName = "image")]
        [Required]
        public byte[] slika { get; set; }

        public int trajanje { get; set; }

        public int pocetnaCena { get; set; }

        public int trenutnaCena { get; set; }

        public DateTime datumVremeStvaranja { get; set; }

        public DateTime datumVremeOtvaranja { get; set; }

        public DateTime datumVremeZatvaranja { get; set; }

        [Required]
        [StringLength(50)]
        public string stanje { get; set; }

        public Guid korisnik { get; set; }

        public virtual korisnik korisnik1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ponuda> ponudas { get; set; }
    }
}
