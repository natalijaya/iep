namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("narudzbina")]
    public partial class narudzbina
    {
        [Key]
        public Guid GUID { get; set; }

        public Guid korisnik { get; set; }

        public int brTokena { get; set; }

        public double cena { get; set; }

        [Required]
        [StringLength(50)]
        public string stanje { get; set; }

        public virtual korisnik korisnik1 { get; set; }
    }
}
