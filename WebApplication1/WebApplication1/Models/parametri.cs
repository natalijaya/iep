namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("parametri")]
    public partial class parametri
    {
        public Guid ID { get; set; }

        public int N { get; set; }

        public int D { get; set; }

        public int S { get; set; }

        public int G { get; set; }

        public int P { get; set; }

        [Required]
        [StringLength(10)]
        public string C { get; set; }

        public double T { get; set; }
    }
}
