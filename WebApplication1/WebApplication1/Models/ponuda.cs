namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ponuda")]
    public partial class ponuda
    {
        public Guid ID { get; set; }

        public Guid korisnik { get; set; }

        public Guid aukcija { get; set; }

        public DateTime datumVremeSlanja { get; set; }

        public int brojTokena { get; set; }

        public virtual aukcija aukcija1 { get; set; }

        public virtual korisnik korisnik1 { get; set; }
    }
}
