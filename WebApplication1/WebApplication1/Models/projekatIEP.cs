namespace WebApplication1.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class projekatIEP : DbContext
    {
        public projekatIEP()
            : base("name=projekatIEP")
        {
        }

        public virtual DbSet<aukcija> aukcijas { get; set; }
        public virtual DbSet<korisnik> korisniks { get; set; }
        public virtual DbSet<narudzbina> narudzbinas { get; set; }
        public virtual DbSet<parametri> parametris { get; set; }
        public virtual DbSet<ponuda> ponudas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<aukcija>()
                .Property(e => e.naziv)
                .IsUnicode(false);

            modelBuilder.Entity<aukcija>()
                .Property(e => e.stanje)
                .IsUnicode(false);

            modelBuilder.Entity<aukcija>()
                .HasMany(e => e.ponudas)
                .WithRequired(e => e.aukcija1)
                .HasForeignKey(e => e.aukcija)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<korisnik>()
                .Property(e => e.ime)
                .IsUnicode(false);

            modelBuilder.Entity<korisnik>()
                .Property(e => e.prezime)
                .IsUnicode(false);

            modelBuilder.Entity<korisnik>()
                .Property(e => e.mejl)
                .IsUnicode(false);

            modelBuilder.Entity<korisnik>()
                .Property(e => e.lozinka)
                .IsUnicode(false);

            modelBuilder.Entity<korisnik>()
                .HasMany(e => e.aukcijas)
                .WithRequired(e => e.korisnik1)
                .HasForeignKey(e => e.korisnik)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<korisnik>()
                .HasMany(e => e.narudzbinas)
                .WithRequired(e => e.korisnik1)
                .HasForeignKey(e => e.korisnik)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<korisnik>()
                .HasMany(e => e.ponudas)
                .WithRequired(e => e.korisnik1)
                .HasForeignKey(e => e.korisnik)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<narudzbina>()
                .Property(e => e.stanje)
                .IsUnicode(false);

            modelBuilder.Entity<parametri>()
                .Property(e => e.C)
                .IsUnicode(false);
        }
    }
}
