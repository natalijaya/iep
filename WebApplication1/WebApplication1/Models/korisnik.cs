namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("korisnik")]
    public partial class korisnik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public korisnik()
        {
            aukcijas = new HashSet<aukcija>();
            narudzbinas = new HashSet<narudzbina>();
            ponudas = new HashSet<ponuda>();
        }

        [Key]
        public Guid UID { get; set; }

        [Required]
        [Display(Name = "Name")]
        [StringLength(50)]
        public string ime { get; set; }

        [Required]
        [Display(Name = "Surname")]
        [StringLength(50)]
        public string prezime { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        [StringLength(50)]
        public string mejl { get; set; }

        [Required]
        [Display(Name = "Password")]
        [StringLength(50)]
        public string lozinka { get; set; }
        
        [Display(Name = "Number of tokens")]
        public int brTokena { get; set; }

        [Display(Name = "Admin")]
        public int admin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<aukcija> aukcijas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<narudzbina> narudzbinas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ponuda> ponudas { get; set; }
    }
}
