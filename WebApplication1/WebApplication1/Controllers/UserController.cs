﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using WebApplication1.Help;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class UserController : Controller
    {
        private projekatIEP db = new projekatIEP();

        // GET: User
        [AuthorizeUser]
        public ActionResult Index()
        {
            return View();
        }

        // GET: User
       // [AuthorizeUser]
        public ActionResult IndexUsers()
        {
            return View(db.korisniks.ToList());
        }

        // GET: User/Details/5
        [AuthorizeUser]
        public ActionResult Details()
        {
            korisnik k = Session["User"] as korisnik;
            if (k == null)
            {
                return HttpNotFound();
            }
            return View(k);
        }

        private string GetMD5HashData(string data)
        {
            //create new instance of md5
            MD5 md5 = MD5.Create();

            //convert the input text to array of bytes
            byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(data));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            // return hexadecimal string
            return returnValue.ToString();

        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UID,ime,prezime,mejl,lozinka")] korisnik model)
        {
            if (ModelState.IsValid)
            {

                if (db.korisniks.Where(u => u.mejl == model.mejl).FirstOrDefault() != null)
                {
                    ViewBag.Message = "Mail adress already exists in the database, please try with another email.";
                    return View(model);
                }

                Guid obj = Guid.NewGuid();
                string pass = GetMD5HashData(model.lozinka);

                korisnik k = new korisnik
                {
                    UID = obj,
                    ime = model.ime,
                    prezime = model.prezime,
                    mejl = model.mejl,
                    lozinka = pass,
                    brTokena = 0,
                    admin = 0
                };

                db.korisniks.Add(k);
                db.SaveChanges();
                return RedirectToAction("Login");
            }

            return View(model);
        }

        // GET: User/Edit/5
        [AuthorizeUser]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            korisnik korisnik = db.korisniks.Find(id);
            if (korisnik == null)
            {
                return HttpNotFound();
            }
            return View(korisnik);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUser]
        public ActionResult Edit([Bind(Include = "UID,ime,prezime,mejl,lozinka")] korisnik model)
        {
            if (ModelState.IsValid)
            {

                korisnik k = Session["User"] as korisnik;
                korisnik kor = db.korisniks.Where(u => u.UID == k.UID).FirstOrDefault();

                kor.ime = model.ime;
                kor.prezime = model.prezime;
                kor.mejl = model.mejl;
                if (kor.admin == 0)
                {
                    string pass = GetMD5HashData(model.lozinka);
                    kor.lozinka = pass;
                }
                else kor.lozinka = model.lozinka;

                db.Entry(kor).State = EntityState.Modified;
                db.SaveChanges();

                Session["User"] = kor;

                return RedirectToAction("Details");
            }
            return View(model);
        }

        // GET: User/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: User/Login
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "mejl,lozinka")] LoginExtended model)
        {
            if (ModelState.IsValid)
            {
                string pass = GetMD5HashData(model.lozinka);

                korisnik k = db.korisniks.Where(u => u.mejl == model.mejl).FirstOrDefault();

                if (k == null)
                {
                    ViewBag.Message = "Email is incorrect.";
                    return View();
                }

                if (k.admin == 0)
                {
                    if (pass == k.lozinka)
                    {
                        Session["User"] = k;
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    if (model.lozinka == k.lozinka)
                    {
                        Session["User"] = k;
                        return RedirectToAction("Index", "Admin");
                    }
                }

                ViewBag.Message = "Password is incorrect.";
                return View();
            }
            return View(model);
        }

        [AuthorizeUser]
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
