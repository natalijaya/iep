﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Help;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AdminController : Controller
    {
        private projekatIEP db = new projekatIEP();

        // GET: Admin
        [AuthorizeAdmin]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin
        [AuthorizeAdmin]
        public ActionResult IndexPar()
        {
            return View(db.parametris.ToList());
        }

        // GET: Admin/Details/5
        [AuthorizeAdmin]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            parametri parametri = db.parametris.Find(id);
            if (parametri == null)
            {
                return HttpNotFound();
            }
            return View(parametri);
        }
        
        // GET: Admin/Edit/5
        [AuthorizeAdmin]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            parametri parametri = db.parametris.Find(id);
            if (parametri == null)
            {
                return HttpNotFound();
            }
            return View(parametri);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeAdmin]
        public ActionResult Edit([Bind(Include = "ID,N,D,S,G,P,C,T")] parametri model)
        {
            if (ModelState.IsValid)
            {
                parametri admin = db.parametris.FirstOrDefault();
                if (admin != null)
                {
                    if (!((model.S < model.G) && (model.G < model.P)))
                    {
                        ViewBag.Message = "Number of tokens must be in the following order: silver<gold<platinum.";
                        return View(model);
                    }

                    admin.N = model.N;
                    admin.D = model.D;
                    admin.S = model.S;
                    admin.G = model.G; 
                    admin.P = model.P;
                    admin.C = model.C; 
                    admin.T = model.T; 

                    db.Entry(admin).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("IndexPar");
                }
            }
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
