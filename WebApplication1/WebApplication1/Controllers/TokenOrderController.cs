﻿using Microsoft.AspNet.SignalR;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Help;
using WebApplication1.Hubs;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TokenOrderController : Controller
    {
        private projekatIEP db = new projekatIEP();

        // GET: TokenOrder
        [AuthorizeUser]
        public ActionResult Index()
        {
            return View();
        }

        // GET: TokenOrder/Details/5
        [AuthorizeUser]
        public ActionResult Details(int? page)
        {
            korisnik user = Session["User"] as korisnik;
            IList<narudzbina> orders = db.narudzbinas.Where(t => t.korisnik == user.UID).ToList();
            parametri admin = db.parametris.FirstOrDefault();

            int pageNumber = (page ?? 1);
            return View(PagedListExtensions.ToPagedList(orders, pageNumber, admin.N));
        }

        // GET: TokenOrder/Create
        [AuthorizeUser]
        public ActionResult Create()
        {
            parametri p = db.parametris.FirstOrDefault();
            return View(p);
        }

        // POST: TokenOrder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUser]
        public ActionResult Create(string tokens)
        {
            narudzbina n = new narudzbina();
            n.GUID = Guid.NewGuid();

            parametri pom = db.parametris.FirstOrDefault();
            if (tokens == "silver") n.brTokena = pom.S;
            else
            if (tokens == "gold") n.brTokena = pom.G;
            else
            if (tokens == "platinum") n.brTokena = pom.P;
            else
            {
                ViewBag.Message = "0";
                return View();
            }


            korisnik k = Session["User"] as korisnik;
            n.korisnik = k.UID;

            n.cena = n.brTokena * pom.T;

            n.stanje = "Submitted";

            db.narudzbinas.Add(n);
            db.SaveChanges();
            return Redirect("http://stage.centili.com/payment/widget?apikey=e37b10434628c1a64bc981084cf4aa51&country=rs&reference=" + n.GUID);
            
        }

        [AuthorizeUser]
        public ActionResult Completed(Guid? reference, string status)
        {
            using (var transaction = db.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    narudzbina order = db.narudzbinas.Where(t => t.GUID == reference).FirstOrDefault();
                    if (order == null)
                        RedirectToAction("Details");
                    if (order.stanje == "Submitted")
                    {
                        if (status == "success")
                        {
                            order.stanje = "Completed";
                            korisnik user1 = Session["User"] as korisnik;
                            korisnik user = db.korisniks.Where(u => u.UID == user1.UID).FirstOrDefault();
                            user.brTokena += order.brTokena;
                            var number = user.brTokena;
                            db.Entry(user).State = EntityState.Modified;
                            db.Entry(order).State = EntityState.Modified;
                            db.SaveChanges();
                            SendEmail(user, "Token order", "Token order successfully completed!");
                            transaction.Commit();

                        }
                        else
                        {
                            order.stanje = "Canceled";
                            db.Entry(order).State = EntityState.Modified;
                            db.SaveChanges();
                            ViewBag.Mesage = "The order is canceled";
                            transaction.Commit();
                        }

                    }
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    ViewBag.Message = "There is an error in transaction";
                
                }
            }
            return RedirectToAction("Details");
        }

        [NonAction]
        public bool SendEmail(korisnik reciver, string subject, string emailBody)
        {
            try
            {
                string senderEmail = System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString();
                string senderPassword = System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString();

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderEmail, senderPassword);
                MailMessage mailMessage = new MailMessage(senderEmail, reciver.mejl, subject, emailBody);
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;

                client.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
