﻿using Microsoft.AspNet.SignalR;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Help;
using WebApplication1.Hubs;
using WebApplication1.Models;
using WebApplication1.Models.Extended;

namespace WebApplication1.Controllers
{
    public class AuctionController : Controller
    {
        private projekatIEP db = new projekatIEP();
        
        // GET: Auction
        [AuthorizeUser]
        public ActionResult IndexUser()
        {
            // var aukcijas = db.aukcijas.Include(a => a.korisnik1);
            // return View(aukcijas.ToList());
            return View();
        }

        // GET: Auction
        [AuthorizeAdmin]
        public ActionResult IndexAdmin()
        {
            return View();
        }

        // GET: Auction
        public ActionResult IndexA(string searchString, string currentFilter, string priceFrom, string priceTo, string itemsPerPage, int? page)
        {
            if (searchString != null)
                page = 1;
            else searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            var allAuctions = db.aukcijas.Where(a => a.stanje.Contains("Ready") == false).OrderByDescending(a => a.datumVremeOtvaranja).OrderByDescending(a => a.stanje).ToList();
                    
            if (!String.IsNullOrEmpty(searchString))
            {
                var str = searchString;
                str = Regex.Replace(str, @"\s+", " ");

                foreach (var word in str.Split(' '))
                {
                    allAuctions = allAuctions.Where(a => a.naziv.ToLower().Contains(word.ToLower())).ToList();
                }
            }

            ViewBag.Min = null;
            int m;
            if (!String.IsNullOrEmpty(priceFrom) && int.TryParse(priceFrom, out m))
            {
                allAuctions = allAuctions.Where(a => a.trenutnaCena >= m).ToList();
                ViewBag.Min = m;
            }

            ViewBag.Max = null;
            if (!String.IsNullOrEmpty(priceTo) && int.TryParse(priceTo, out m))
            {
                allAuctions = allAuctions.Where(a => a.trenutnaCena <= m).ToList();
                ViewBag.Max = m;
            }

            int items;
            parametri p = db.parametris.FirstOrDefault();
            ViewBag.N = (int.TryParse(itemsPerPage, out items) && items > 0) ? items : p.N;


            int pageNumber = (page ?? 1);
           
            return View(allAuctions.ToPagedList(pageNumber, (int)ViewBag.N));

        }

        // GET: Auction/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aukcija aukcija = db.aukcijas.Find(id);
            if (aukcija == null)
            {
                return HttpNotFound();
            }
            return View(aukcija);
        }

        // GET: Auction/Create
        [AuthorizeUser]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Auction/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUser]
        public ActionResult Create([Bind(Include = "naziv,trajanje,pocetnaCena,imagePath, imageFile")] AuctionCreateExtended model)
        {
            // @Html.ValidationMessageFor(model => model.imagePath, "", new { @class = "text-danger" })
            string FileName = Path.GetFileNameWithoutExtension(model.imageFile.FileName);
            string extension = Path.GetExtension(model.imageFile.FileName);
            model.imagePath = FileName + extension;
            if (ModelState.IsValid)
            {
                aukcija a = new aukcija();
                a.GUID = Guid.NewGuid();
                a.naziv = model.naziv;
                byte[] slika = new byte[model.imageFile.ContentLength];
                model.imageFile.InputStream.Read(slika, 0, slika.Length);
                a.slika = slika;
                a.trajanje = model.trajanje;
                parametri pom = db.parametris.FirstOrDefault();
                a.pocetnaCena = model.pocetnaCena * ((int)pom.T);
                a.trenutnaCena = model.pocetnaCena * ((int)pom.T);
                a.datumVremeStvaranja = DateTime.Now;
                a.datumVremeOtvaranja = DateTime.Now;
                a.datumVremeZatvaranja = DateTime.Now;
                a.stanje = "Ready";
                korisnik k = Session["User"] as korisnik;
                if (k == null)
                {
                    ViewBag.Message = "User";
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                a.korisnik = k.UID;

                db.aukcijas.Add(a);
                db.SaveChanges();
                return RedirectToAction("IndexA");
            }
            ViewBag.Message = "Nije";
            return View(model);
        }

        [AuthorizeAdmin]
        public ActionResult NeedApproval(int? page)
        {
            var aukcije = db.aukcijas.Where(a => a.stanje.Equals("Ready")).ToList();

            parametri p = db.parametris.FirstOrDefault();
            int items = p.N;

            int pageNumber = (page ?? 1);
            return View(aukcije.ToPagedList(pageNumber, items));
        }

        [AuthorizeAdmin]
        public ActionResult OpenAuction(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aukcija aukcija = db.aukcijas.Find(id);
            if (aukcija == null)
            {
                return HttpNotFound();
            }

            aukcija.stanje = "Opened";
            aukcija.datumVremeOtvaranja = DateTime.Now;

            aukcija.datumVremeZatvaranja = DateTime.Now.AddMilliseconds(aukcija.trajanje * 60 * 1000) ;

            db.Entry(aukcija).State = EntityState.Modified;
            db.SaveChanges();

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<BidHub>();
            string notification = "New auction just started. If you don't see it please click <a href = " + Url.Action("Index", "Auction", null) + ">here</a>";
            hubContext.Clients.All.newAuction(notification);

            return RedirectToAction("NeedApproval");
        }

        [AuthorizeAdmin]
        public ActionResult RejectAuction(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aukcija aukcija = db.aukcijas.Find(id);
            if (aukcija == null)
            {
                return HttpNotFound();
            }

            db.aukcijas.Remove(aukcija);
            db.SaveChanges();

            return RedirectToAction("NeedApproval");
        }
        
        [AuthorizeUser]
        public ActionResult WonAuctions(int? page, Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            aukcija aukcija = db.aukcijas.Where(a => a.korisnik1.UID == id).FirstOrDefault();
            if (aukcija == null)
            {
                return HttpNotFound();
            }

            List<aukcija> dobijeneAukcije = new List<aukcija>();
            var aukcije = db.aukcijas.Where(a => a.stanje.Equals("Completed")).ToList();
            foreach (var a in aukcije)
            {
                if (a.korisnik1.UID == id)
                    dobijeneAukcije.Add(a);
            }

            parametri p = db.parametris.FirstOrDefault();
            int n = p.N;
            int brStrane = (page ?? 1);

            return View(dobijeneAukcije.ToPagedList(brStrane, n));
        }

        [AuthorizeUser]
        public async Task<ActionResult> Bidding(Guid? auctionID, int? bidOffer)
        {
            korisnik user = Session["User"] as korisnik;

            ViewBag.Message = "usao bpre";
            using (var transaction = db.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
            {
                try
                {
                    aukcija auction = db.aukcijas.Where(a => a.GUID.Equals(auctionID) && a.stanje != "Completed").FirstOrDefault();
                    if (auction == null) return View();
                    if (bidOffer == 0) bidOffer += 1;
                    var tokens = auction.trenutnaCena + bidOffer;

                    ponuda previousBid = db.ponudas.Where(p => p.aukcija == auction.GUID).OrderByDescending(p => p.brojTokena).FirstOrDefault();
                    korisnik previousBidder = null;
                    korisnik returnTokensAccount = null;
                    int currentPriceInTokens;
                    if (previousBid != null)
                    {
                        previousBidder = db.korisniks.Find(previousBid.korisnik);
                        currentPriceInTokens = previousBid.brojTokena;
                    }
                    else
                        currentPriceInTokens = auction.trenutnaCena - 1;

                    if (currentPriceInTokens + 1 >= tokens)
                    {
                        ViewBag.Message = "Your bid is too small";
                        return View();
                    }

                    if (user.brTokena - currentPriceInTokens - tokens <= 0)
                    {

                        ViewBag.Message = "You do not have tokens";
                        return View();
                    }

                    ponuda leadingBid = new ponuda
                    {
                        korisnik = user.UID,
                        aukcija = auction.GUID,
                        datumVremeSlanja = System.DateTime.Now,
                        brojTokena = (int)tokens
                    };

                    if (previousBidder != null)
                        returnTokensAccount = previousBidder;

                    if (returnTokensAccount != null)
                    {
                        returnTokensAccount.brTokena += previousBid.brojTokena;
                        db.Entry(returnTokensAccount).State = EntityState.Modified;
                    }

                    user.brTokena -= leadingBid.brojTokena;
                    db.Entry(user).State = EntityState.Modified;

                    Session["User"] = user;

                    db.ponudas.Add(leadingBid);
                    auction.trenutnaCena = leadingBid.brojTokena;
                    db.Entry(auction).State = EntityState.Modified;
                    await db.SaveChangesAsync();

                    transaction.Commit();

                    var hubContext = GlobalHost.ConnectionManager.GetHubContext<BidHub>();

                    hubContext.Clients.All.updateAuction(auction.GUID, auction.trenutnaCena, user.mejl);
                    hubContext.Clients.All.newBid(user.mejl, System.DateTime.Now.ToString("d/M/yyyy"), System.DateTime.Now.ToString("hh:mm:ss"), leadingBid.brojTokena);

                    if (returnTokensAccount != null && returnTokensAccount.UID != user.UID)
                    {
                        hubContext.Clients.All.returnTokens(returnTokensAccount.UID, returnTokensAccount.brTokena);
                        string notification = "You are outbided";
                        hubContext.Clients.All.returnTokensNotifications(returnTokensAccount.UID, notification);
                    }
                    //skace se na detalje o aukciji o kojoj se radi
                    return View();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return RedirectToAction("IndexA");
                }

            }
        }
            

        public void CompleteAuction(Guid? id)
        {
            if (id.HasValue)
            {
                using (var transaction = db.Database.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        aukcija a = db.aukcijas.Find(id);
                        if (a != null && !a.stanje.Equals("Completed"))
                        {
                            a.datumVremeZatvaranja = System.DateTime.Now;
                            a.stanje = "Completed";

                            db.Entry(a).State = EntityState.Modified;
                            db.SaveChanges();

                            SendEmail(a.korisnik1,"Auction result", "Congrats you won");

                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }

            }
        }

        [NonAction]
        public bool SendEmail(korisnik reciver, string subject, string emailBody)
        {
            try
            {
                string senderEmail = System.Configuration.ConfigurationManager.AppSettings["SenderEmail"].ToString();
                string senderPassword = System.Configuration.ConfigurationManager.AppSettings["SenderPassword"].ToString();

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(senderEmail, senderPassword);

                MailMessage mailMessage = new MailMessage(senderEmail, reciver.mejl, subject, emailBody);
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;

                client.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
