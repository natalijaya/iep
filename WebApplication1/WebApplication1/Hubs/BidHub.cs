﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace WebApplication1.Hubs
{
    [Microsoft.AspNet.SignalR.Hubs.HubName("bidHub")]
    public class BidHub: Hub
    {
        public void UpdateAuction(string idAuction, string newPrice, string leadingBidder)
        {
            Clients.All.updateAuction(idAuction, newPrice, leadingBidder);
        }

        public void NewBid(string leadingBidder, string date, string time, string newPrice)
        {
            Clients.All.newBid(leadingBidder, date, time, newPrice);
        }

        public void ReturnTokens(int previousBidder, string tokens)
        {
            Clients.All.returnTokens(previousBidder, tokens);
        }
        
        public void ReturnTokensNotifications(int previousBidder, string notify)
        {
            Clients.All.returnTokensNotifications(previousBidder, notify);
        }
    }
}